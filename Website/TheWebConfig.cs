﻿using System;
using System.IO;
using System.Web.Configuration;
using System.Xml.Linq;
using System.Xml.XPath;
using Netteller.Core.Errors;

namespace LaunchSitecore
{
	public static class TheWebConfig
	{
		public const string RelKuduSettingFileUri = "../deployments/settings.xml";

		private const string StoreConfigName = "Store";

		private static string _connectionString;

		/// <summary>
		/// Init by reading the web config, optionally provide the current site root path to
		/// allow Kudu settings to override 
		/// </summary>
		/// <param name="appPath"></param>
		public static void InitFromWebConfig(string appPath = null)
		{
			var configSetting = TryReadSettingFromKuduConfig(appPath, StoreConfigName);

			if (configSetting != null) {
				_connectionString = configSetting;
				return;
			}

			configSetting = WebConfigurationManager.AppSettings[StoreConfigName];

			if (configSetting == null) {
				throw new ApplicationError("The Web.Config must include a connection string called: {0}", StoreConfigName);
			}

			_connectionString = configSetting;

			if (String.IsNullOrWhiteSpace(_connectionString)) {
				throw new ApplicationError("Store connection string is required, check Web.Config has {0} in connection strings.");
			}
		}

		public static void InitExplicit(string connectionString)
		{
			if (String.IsNullOrWhiteSpace(connectionString)) {
				throw new ApplicationError("Store connection string is required.");
			}

			_connectionString = connectionString;
		}

		public static void Reset()
		{
			_connectionString = null;
		}

		public static string StoreConnectionString
		{
			get
			{

				if (_connectionString == null) {
					throw new ApplicationError("TheWebConfig has not been initalized, ensure host startup calls initalize");
				}

				return _connectionString;
			}
		}

		private static string TryReadSettingFromKuduConfig(string appPath, string key)
		{
			if (!String.IsNullOrEmpty(appPath)) {

				var hostConfigFile = Path.Combine(appPath, RelKuduSettingFileUri);

				if (File.Exists(hostConfigFile)) {

					var doc = XDocument.Load(hostConfigFile);

					var storeSetting = doc.XPathSelectElement(String.Format("//add[@key='{0}']", key));

					if (storeSetting != null) {

						var value = storeSetting.Attribute("value");
						if (value != null) {

							return value.Value;
						}
					}
				}
			}
			return null;
		}
	}
}