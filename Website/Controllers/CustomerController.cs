﻿using System.Collections.Generic;
using System.Linq;
using LaunchSitecore.Configuration.SiteUI.Base;
using LaunchSitecore.Models;
using System.Web.Mvc;
using System.Threading.Tasks;
using Netteller.Domain.Models;
using Raven.Client;

namespace LaunchSitecore.Controllers
{
    //[Authorize(Roles = SecurityExtensions.MemberRole)]
    public class CustomerController : LaunchSitecoreBaseController
    {
        private readonly IAsyncDocumentSession _asyncDocumentSession;

        public CustomerController(IAsyncDocumentSession asyncDocumentSession)
        {
            _asyncDocumentSession = asyncDocumentSession;
        }

        public ActionResult AccountsSync()
        {
            ActionResult result = null;
            var task = Task.Run(async () =>
            {
                result = await Accounts().ConfigureAwait(false);
            });

            task.Wait();
            return result;
        }

        public async Task<ActionResult> Accounts()
        {
            var customerNumber = "BIZ009";

            var customer = await _asyncDocumentSession.LoadAsync<Customer>(customerNumber);

            return customer != null ? PartialView("~/Views/Customer/AccountsSync.cshtml", MapAccountsToViewModel(customer)) : PartialView("~/Views/Customer/AccountsSync.cshtml");
        }

        private List<AccountViewModel> MapAccountsToViewModel(Customer customer)
        {
            return customer.Accounts.Select(account => new AccountViewModel
            {
                AccountType = account.AccountType,
                BSB = account.BSB,
                AvailableBalance = account.AvailableBalance,
                CurrentBalance = account.CurrentBalance,
                AccountName = account.AccountName,
                AccountNumber = account.AccountId
            }).ToList();
        }
    }
}