﻿using System.Linq;
using LaunchSitecore.Configuration.SiteUI.Base;
using LaunchSitecore.Models;
using System.Web.Mvc;
using System.Threading.Tasks;
using LaunchSitecore.Services;
using LaunchSitecore.Handlers.HostTopic;
using Netteller.Core.AspNet;
using Netteller.Core.Bus;
using Netteller.Core.Owin.Prg;
using Netteller.Core.Singletons;
using Raven.Client;
using Raven.Client.Document;

namespace LaunchSitecore.Controllers
{
    public class DatabaseController : LaunchSitecoreBaseController
    {
        private readonly IDocumentStore _store;
        private readonly HostTopics _topics;
        private readonly IMessageHandlerLoop _messgeHandlerLoop;
        private readonly IHostBus _bus;
        private readonly TheSolutionEnvironment _solutionConfig;
        private readonly TheWebSiteSetup _siteConfig;
        private readonly IPrgMessages _prgMessages;


        public DatabaseController(
            IDocumentStore store,
            HostTopics topics,
            IMessageHandlerLoop messgeHandlerLoop,
            IHostBus bus,
            TheSolutionEnvironment solutionConfig,
            TheWebSiteSetup siteConfig,
            IPrgMessages prgMessages)
        {
            _store = store;
            _topics = topics;
            _messgeHandlerLoop = messgeHandlerLoop;
            _bus = bus;
            _solutionConfig = solutionConfig;
            _siteConfig = siteConfig;
            _prgMessages = prgMessages;
        }

        public ActionResult BottomWidget()
        {
            return PartialView("~/Views/Tswg/BottomWidget.cshtml", new BottomWidget(DataSourceItem));
        }

        public ActionResult SiteSummary()
       {
           ActionResult result = null;
           var task = Task.Run(async () =>
           {
               result = await SiteSummaryAsync().ConfigureAwait(false);
           });
                
            task.Wait();
            return result;

        }

        [HttpPost]
        public string TrySave(string id)
        {
            _store.Initialize();
            var TestSend = new TestSend
            {
                id = id,
                Notes = "Test Send - " + id,
                Title = "Test Send Title - " + id,
                URL = "abc@abc.com"
            };
            using (var session = _store.OpenSession())
            {
                session.Store(TestSend);
                session.SaveChanges();
            }

            return id;
        }

        [HttpGet]
        public string TryGet()
        {
            _store.Initialize();
            var session = _store.OpenSession();

            var result = session.Query<TestSend>();
            var resultJson = "{ \"data\": [";
            foreach (var testSend in result)
            {
                resultJson += Newtonsoft.Json.JsonConvert.SerializeObject(testSend) + ", ";
            }
            resultJson = resultJson.Substring(0, resultJson.Length - 2);
            resultJson += "]}";
            return resultJson;
        }

        public async Task<ActionResult> SiteSummaryAsync()
        {
            
            var model = new SystemStatusViewModel();

            model.PublicUrl = _solutionConfig.WebSiteUrl;
            model.Environment = _solutionConfig.EnvironmentName;

            model.AuthServerUrl = _siteConfig.TryGetAuthServerPublicUrl() ?? "undefined";

            dynamic dynamicDocumentStoreProxy = _store;

            var serverStore = dynamicDocumentStoreProxy.__interceptors[0].Instance as DocumentStore;

            if (serverStore != null)
            {
                model.RavenServerDbName = serverStore.DefaultDatabase;
                model.RavenServerUri = serverStore.Url;
                model.RavenStudioUri = "{" + serverStore.Url + "}/studio/index.html#databases/documents?&database={" + serverStore.DefaultDatabase + "}";
            }
            model.IsEmbededStore = serverStore == null;

            model.BusNamespace = _bus.Namespace;
            model.IsEmbededBus = _bus.IsEmbedded;

            model.DomainTopicName = _topics.Domain.Name;

            model.HostTopicListenerStatus = _messgeHandlerLoop.IsRunning ? "Running" : "Paused";

            var hostListener = _messgeHandlerLoop.Handlers.OfType<HostTopicHandler>().Single();
            model.HostTopicHandled = hostListener.HandledCount;

            model.HostListenerName = hostListener.Name;
            if (hostListener.SubscriptionChannel != null)
            {
                var sub = hostListener.SubscriptionChannel;

                await sub.Refresh();
                model.HostListenerPending = sub.PendingMessageCount;
                model.HostListenerDead = sub.DeadMessageCount;
            }
            
            return PartialView(model);
        }
    }
}

