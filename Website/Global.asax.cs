﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Raven.Abstractions.Logging;

namespace LaunchSitecore
{
	public class MvcApplication : Sitecore.Web.Application
    {
		protected void Application_Start()
		{
			TheWebConfig.InitFromWebConfig(Server.MapPath("~/"));

            AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
		RouteConfig.RegisterRoutes(RouteTable.Routes);
//			BundleConfig.RegisterBundles(BundleTable.Bundles);
		}

		protected void Application_Error()
		{
			var error = Server.GetLastError();
			var logger = LogManager.GetCurrentClassLogger();
			
			logger.FatalException("Application Unhandled Error", error);
		}
	}
}
