﻿namespace LaunchSitecore.Events
{
	public class HostShutdown
	{
		public string Name { get; set; }
	}
}