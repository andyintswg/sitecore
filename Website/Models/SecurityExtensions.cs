﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using Netteller.Core.Security;

namespace LaunchSitecore.Models
{
	public static class SecurityExtensions
	{
		public const string AdminRole = "admin";
		public const string TwsgStaffRole = "support";
		public const string ClientStaffRole = "client";
		public const string MemberRole = "member";

		public static readonly KeyValuePair<string,string> AutoAssignedInbuiltUserClaim = 
			new KeyValuePair<String, String>("autoInbuiltUser", "true");

		public static readonly string[] AllKnownRoles = {
			AdminRole,
			TwsgStaffRole,
			ClientStaffRole,
			MemberRole
		};

		public static bool IsBuiltin(this IPrincipal principal)
		{
			return principal != null &&
					principal.Identity != null &&
				   principal.Identity.IsBuiltin();
		}

		public static bool IsBuiltin(this IIdentity identity)
		{
			var claimsIdentity = identity as ClaimsIdentity;

			return claimsIdentity != null &&
				   claimsIdentity.HasClaim(c => c.Type == AutoAssignedInbuiltUserClaim.Key);
		}

		public static bool IsAdmin(this IPrincipal principal)
		{
			return principal.HasRole(AdminRole);
		}

		public static bool IsTswgStaff(this IPrincipal principal)
		{
			return principal.HasRole(TwsgStaffRole);
		}

		public static bool IsClientStaff(this IPrincipal principal)
		{
			return principal.HasRole(ClientStaffRole);
		}

		public static bool IsMember(this IPrincipal principal)
		{
			return principal.HasRole(MemberRole);
		}
	}
}