﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LaunchSitecore.Configuration.SiteUI;

namespace LaunchSitecore.Models
{
    public class BottomWidget : CustomItem
    {
        public BottomWidget(Item item)
      : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public string Title
        {
            get { return InnerItem[FieldId.Title]; }
        }

        public string Content
        {
            get { return InnerItem[FieldId.Content]; }
        }

        public string Image
        {
            get { return InnerItem[FieldId.Image]; }
        }

        public string LinkText
        {
            get { return InnerItem[FieldId.LinkText]; }
        }

        public string LinkToUrl
        {
            get { return null; }
        }

        public Item Item
        {
            get { return InnerItem; }
        }

        public static class FieldId
        {
            public static readonly ID Title = new ID("{2EDD30A6-2D20-4CA5-B0ED-96AEF8E2F305}");
            public static readonly ID Content = new ID("{8BC46D78-CBB5-43F9-B942-9D6AF520606F}");
            public static readonly ID Image = new ID("{182F61C5-0791-4305-B5B2-5AA785B5A96E}");
            public static readonly ID LinkText = new ID("{39B67888-5C9E-441B-A953-47A4F666F67D}");
            public static readonly ID Button = new ID("{652E9D87-4374-4837-92B6-41FC74D41CFA}");

        }
    }
}