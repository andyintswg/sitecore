﻿namespace LaunchSitecore.Models
{
	public class SystemStatusViewModel
	{
		public SystemStatusViewModel()
		{
			IsEmbededBus = true;
			HostListenerDead = 0;
			HostListenerPending = 0;
			HostListenerName = "unknown";
			HostTopicListenerStatus = "unknown";

			IsEmbededStore = true;
			RavenServerUri = "unknown";
			RavenServerDbName = "unknown";
			RavenStudioUri = "unknonw";
		}

		public string Environment { get; set; }
		public string PublicUrl { get; set; }

		public string AuthServerUrl { get; set; }

		public bool IsEmbededStore { get; set; }

		public string RavenServerUri { get; set; }
		public string RavenStudioUri { get; set; }
		public string RavenServerDbName { get; set; }

		public bool IsEmbededBus { get; set; }

		public string BusNamespace { get; set; }

		public string HostTopicListenerStatus { get; set; }
		public string HostListenerName { get; set; }
		public long HostListenerPending { get; set; }
		public long HostListenerDead { get; set; }
		public long HostTopicHandled { get; set; }

		public string DomainTopicName { get; set; }
	}
}