﻿namespace LaunchSitecore.Models
{
    public class AccountViewModel
    {
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public double AvailableBalance { get; set; }
        public double CurrentBalance { get; set; }
        public string AccountType { get; set; }
    }
}