﻿using System;
using Netteller.Core.NLog;
using Netteller.Core.Owin.OAuth;
using Netteller.Core.Singletons;

namespace LaunchSitecore.Models
{
	public class TheWebSiteSetup : IHostSingleton, IDocumentSingleton
	{
		public TheWebSiteSetup()
		{
			UseLocalAuthServer = false;
			LocalAuthServerOptions = new AuthOptions("builtin", "231f025ae977625747fd4d3f60934bbf");

			Logging = new LoggingOptions();
		}

		/// <summary>
		/// Flag to indicate if site should use auth server.
		/// If no auth server, will fall-back to inbuilt security.
		/// </summary>
		/// <remarks>
		/// todo: this should be part of the core <see cref="TheSolutionEnvironment"/>,
		/// It is fundamental enough that it doesn't belong on per-host.
		/// </remarks>
		public bool UseLocalAuthServer { get; set; }

		public AuthOptions LocalAuthServerOptions { get; private set; }

		public LoggingOptions Logging { get; private set; }

		public string TryGetAuthServerPublicUrl()
		{
			if (LocalAuthServerOptions == null || LocalAuthServerOptions.AuthServerEndpoint == null) { return null; }

			var uri = new UriBuilder(LocalAuthServerOptions.AuthServerEndpoint) { Path = "" };

			return uri.ToString();
		}

		public Boolean AfterLoad()
		{
			return false;
		}
	}
}