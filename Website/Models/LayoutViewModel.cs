﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Netteller.Core.Errors;

namespace LaunchSitecore.Models
{
	/// <summary>
	/// Session only view model, used by site layout
	/// </summary>
	public class LayoutViewModel
	{
		private static readonly string SessionKey = typeof(LayoutViewModel).Name;

		public static LayoutViewModel Default(Controller forController)
		{
			if (forController == null) {throw new ArgumentNullException("forController");}

			return Find(forController.Session) ?? Create(forController.Session);
		}

		public static LayoutViewModel Default(WebViewPage forView)
		{
			if (forView == null) {throw new ArgumentNullException("forView");}

			return Find(forView.Session) ?? Create(forView.Session);
		}

		public static LayoutViewModel Default(HttpContextBase forContext)
		{
			if (forContext == null) { throw new ArgumentNullException("forContext"); }

			return Find(forContext.Session) ?? Create(forContext.Session);
		}

		public static bool Exists(HttpContextBase forContext)
		{
			if (forContext == null) {throw new ArgumentNullException("forContext");}

			return Find(forContext.Session) != null;
		}

		private LayoutViewModel()
		{}

		private static LayoutViewModel Find(HttpSessionStateBase session)
		{
			var found = session[SessionKey];

			if (found != null) {

				var model = found as LayoutViewModel;
				if (model == null) {
					throw new ApplicationException("The sesion variable " + SessionKey + " is reserved");
				}

				return model;
			}

			return null;
		}

		private static LayoutViewModel Create(HttpSessionStateBase session)
		{
			if (session[SessionKey] != null) { throw new ApplicationError("Cannot create new LayoutViewModel, the session already has an object.");}

			var newItem = new LayoutViewModel();
			session[SessionKey] = newItem;

			return newItem;
		}
	}
}