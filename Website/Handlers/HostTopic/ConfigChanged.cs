﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;
using Netteller.Core.Bus;
using Newtonsoft.Json.Linq;

namespace LaunchSitecore.Handlers.HostTopic
{
	public class ConfigChanged : IEventHandler
	{
		public String ForEvent {get { return GetType().Name; } }

		public Task<Boolean> Run(JObject @event, CancellationToken token)
		{
			token.ThrowIfCancellationRequested();

			HostingEnvironment.InitiateShutdown();

			return Task.FromResult(true);
		}
	}
}