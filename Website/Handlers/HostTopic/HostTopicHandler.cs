﻿using System.Net;
using Netteller.Core.Bus;
using Netteller.Core.Bus.HandleEvents;

namespace LaunchSitecore.Handlers.HostTopic
{
	public class HostTopicHandler : MessageHandler
	{
		public HostTopicHandler(ITopic owner)
			: base(CreateNameFromMachine(), owner)
		{
			AddHandler(() => new ConfigChanged());
		}

		private static string CreateNameFromMachine()
		{
				return Dns.GetHostName();
		}
	}
}