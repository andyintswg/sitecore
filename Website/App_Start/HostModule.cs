﻿using System;
using System.Web;
using System.Web.Mvc;
using Netteller.Core.AspNet;
using Netteller.Core.Bus;
using Netteller.Core.Bus.Embedded;
using Netteller.Core.Bus.ServiceBus;
using Netteller.Core.Mvc.Filters;
using Netteller.Core.Ninject.Interceptor;
using Netteller.Core.RavenDb;
using Netteller.Core.RavenDb.Errors;
using Netteller.Core.Singletons;
using Netteller.Domain.Domain;
using Netteller.Domain.Services;
using LaunchSitecore.Handlers.HostTopic;
using LaunchSitecore.Models;
using LaunchSitecore.Services;
using LaunchSitecore.Services.HttpModules;
using Ninject;
using Ninject.Extensions.Interception.Infrastructure.Language;
using Ninject.Modules;
using Ninject.Web.Common;
using Ninject.Web.Mvc.FilterBindingSyntax;
using NLog;
using Raven.Client;
using Raven.Client.Document;

namespace LaunchSitecore
{
	public class HostModule : NinjectModule
	{
		public override void Load()
		{
			StorageServices();

			BusServices();

			NamedTopics();

			HttpModules();

			HostSingletons();
			
			MessageHandlers();

			Logging();

		    MvcFilters();
		}

	    private void Logging()
		{
			Kernel.Bind<ILogger>().ToMethod(ctx => {

				var target = ctx.Request.Target?.Member?.DeclaringType;

				return LogManager.GetLogger(target?.Name ?? "");
			});
		}

		private void StorageServices()
		{
			Kernel.Bind<IDocumentStore>().ToMethod(ctx =>
			{
				var store = new DocumentStore();

				try
				{
					store.ParseConnectionString(TheWebConfig.StoreConnectionString);
				}
				catch (ArgumentException ex)
				{
					throw new BadRavenConnectionString(TheWebConfig.StoreConnectionString, ex);
				}

				RavenSingletonConventions.Apply(store);

				new InjectableJsonResolver(Kernel)
					.Include<IAggregateRoot>()
					.Install(store.Conventions);

				RavenCustomJsonSerializer.Install(store);

				return store.Initialize();
			})
			.InSingletonScope()
			.Intercept().With<ServiceTiming>();

			Kernel.Bind<IAsyncDocumentSession>()
			.ToMethod(c => c.Kernel.Get<IDocumentStore>().OpenAsyncSession())
			.InRequestScope()
			.Intercept().With<ServiceTiming>();
		}

		private void BusServices()
		{
			Kernel.Bind<IHostBus>().ToMethod(ctx =>
			{
				var env = ctx.Kernel.Get<TheSolutionEnvironment>();
				return env.DisableBusIntergration
					? (IHostBus)new EmbeddedHostBus()
					: new HostBus(env.ServiceBusConnection);

			}).InSingletonScope();

			Kernel.Bind<IPublisher>().ToMethod(ctx =>
				ctx.Kernel.Get<ITopic>(HostTopics.HostTopic).GetPublisher()
			)
			.Intercept().With<ServiceTiming>();

			Kernel.Bind<IEventForwarder, IDomainPublish>().ToMethod(ctx => new DomainPublish(
					ctx.Kernel.Get<ITopic>(HostTopics.DomainTopic).GetPublisher()	
					)
			)
			.InRequestScope();
		}

		private void NamedTopics()
		{
			Action<IKernel, string> bindNamedTopic = (kernel, name) =>
				kernel.Bind<ITopic>().ToMethod(ctx =>
				{
					var env = kernel.Get<TheSolutionEnvironment>();
					return env.DisableBusIntergration
						? (ITopic)new EmbeddedTopic(name)
						: new Topic((HostBus)kernel.Get<IHostBus>(), name);
				})
				.InSingletonScope()
				.Named(name);

			bindNamedTopic(Kernel, HostTopics.DomainTopic);
			bindNamedTopic(Kernel, HostTopics.HostTopic);
			bindNamedTopic(Kernel, HostTopics.AuthServiceTopic);
			bindNamedTopic(Kernel, HostTopics.CoreBankingService);

			Kernel.Bind<HostTopics>().ToMethod(ctx => new HostTopics(
				ctx.Kernel.Get<ITopic>(HostTopics.DomainTopic),
				ctx.Kernel.Get<ITopic>(HostTopics.HostTopic),
				ctx.Kernel.Get<ITopic>(HostTopics.AuthServiceTopic),
				ctx.Kernel.Get<ITopic>(HostTopics.CoreBankingService)
				));
		}

		private void HttpModules()
		{
			Kernel.Bind<IMessageHandlerLoop, IHttpModule>().To<TheMessageHandlerLoop>().InSingletonScope();
			
			Kernel.Bind<IHttpModule>().To<HostLifecycleEvents>();

			Kernel.Bind<IHttpModule>().To<ConfigureLogging>();

			Kernel.Bind<IHttpModule>().To<ConfigureAntiForgery>();
		}

		private void MessageHandlers()
		{
			Kernel.Bind<IMessageHandler>().ToMethod(ctx => new HostTopicHandler(ctx.Kernel.Get<ITopic>(HostTopics.HostTopic)))
					.InSingletonScope();
		}

		private void HostSingletons()
		{
			Kernel.Bind<TheSolutionEnvironment>()
				.ToMethod(ctx => DocumentSingleton.LoadOrInit<TheSolutionEnvironment>(ctx.Kernel.Get<IDocumentStore>()))
				.InSingletonScope();

			Kernel.Bind<TheWebSiteSetup>()
				.ToMethod(ctx => DocumentSingleton.LoadOrInit<TheWebSiteSetup>(ctx.Kernel.Get<IDocumentStore>()))
				.InSingletonScope();
		}

        private void MvcFilters()
        {
			Kernel.BindFilter<ControllerTiming>(FilterScope.Global, null);
        }
    }
}