﻿using System.Web;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Netteller.Core.AspNet;
using Netteller.Core.Ninject;
using Netteller.Core.Owin.Prg;
using Ninject;
using Ninject.Modules;

namespace LaunchSitecore
{
	public class AspNetModule : NinjectModule
	{
		public override void Load()
		{
			Bind<IOwinContext>().ToMethod(ctx => HttpContext.Current.GetOwinContext())
				.InTransientScope();

			Bind<IAuthenticationManager>().ToMethod(ctx => HttpContext.Current.GetOwinContext().Authentication)
				.InTransientScope();

			Bind<IHostingEnvironment>().To<DefaultHostingEnvironment>()
				.InSingletonScope();
			
			Kernel.Load<PrgModule>();
		}
	}

}