﻿using System.Web.Mvc;
using System.Web.Routing;

namespace LaunchSitecore
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Save",
                url: "Database/TrySave/{id}",
                defaults: new
                {
                    controller = "Database", action = "TrySave", id= UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "Get",
                url: "Database/TryGet",
                defaults: new { controller = "Database", action = "TryGet"}
            );

            //routes.MapRoute(
            //    name: "test",
            //    url: "signin-localauth",
            //    defaults: new { controller = "Home", action = "Index" }
            //);

        }
	}
}
