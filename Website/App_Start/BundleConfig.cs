﻿using System.Web;
using System.Web.Optimization;

namespace LaunchSitecore
{
	public class BundleConfig
	{
		public static class Scripts
		{
			public const string JQuery = "~/bundles/jquery";
			public const string Bootstrap = "~/bundles/bootstrap";
		}

		public static class Styles
		{
			public const string Bootstrap = "~/Content/bootstrapstyle";
			public const string Site = "~/Content/site";
		}

		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle(Scripts.JQuery)
				.Include("~/Scripts/jquery-{version}.js")
				.Include("~/Scripts/jquery.validate*"));

			bundles.Add(new ScriptBundle(Scripts.Bootstrap)
				.Include("~/Scripts/bootstrap.js"));

			bundles.Add(new StyleBundle(Styles.Bootstrap)
				.Include("~/Content/bootstrap.css"));

			bundles.Add(new StyleBundle(Styles.Site)
				.Include("~/Content/site.css"));
		}
	}
}
