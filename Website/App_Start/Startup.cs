﻿using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Logging;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Netteller.Core.Owin.Logging;
using LaunchSitecore;
using LaunchSitecore.Models;
using Netteller.Core.NLog;
using Owin;
using Netteller.Core.Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace LaunchSitecore
{
	public partial class Startup
	{
		private ILogger _log;

		// ReSharper disable once UnusedMember.Global Used by OWIN convention.
		public void Configuration(IAppBuilder app)
		{
			AddMemlogForDebugHelper(app);
			
			//todo: OWIN doesn't yet have an acceptable IoC approach; vNext adds this.
			var container = DependencyResolver.Current;

		    app.UseNLog(new LoggingOptions());

			_log = app.CreateLogger<Startup>();

			_log.WriteInformation("Installing PRG Messages");
			app.UsePrgMessages();

			ConfigureAuth(app, container);
		}

		private void AddMemlogForDebugHelper(IAppBuilder app)
		{
			//debug helper; use mem logger, allong with diag middleware
			MemLog.Install(app);
			app.Use(async (ctx, next) =>
			{
				//break here to see incomming.
				var pre = ctx.Request;

				await next();

				//break here to see outgoing.
				var logs = MemLog.Instance.Value.FindEntries();
				var post = ctx.Response;
			});
		}

		private void ConfigureAuth(IAppBuilder app, IDependencyResolver cont)
		{
			// Application main Authentication.

			_log.WriteVerbose("Using cookie auth as main auth : {" + DefaultAuthenticationTypes.ApplicationCookie + "}");

			app.UseCookieAuthentication(new CookieAuthenticationOptions {
				AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
				CookieName = "web." + DefaultAuthenticationTypes.ApplicationCookie,
				AuthenticationMode = AuthenticationMode.Active,
				LoginPath = new PathString("/Home"),
				Provider = new CookieAuthenticationProvider {
					OnValidateIdentity = (ctx) =>
					{
						var roleType = ctx.Identity.RoleClaimType;
						
						//admin gets all roles.
						var hasAdminRole = ctx.Identity.HasClaim(roleType, SecurityExtensions.AdminRole);
						if (hasAdminRole) {
							_log.WriteInformation("Auth: {" + DefaultAuthenticationTypes.ApplicationCookie + "} have admin user; auto assigning all roles");
							foreach (var role in SecurityExtensions.AllKnownRoles) {
								if (!ctx.Identity.HasClaim(roleType, role)) {
									ctx.Identity.AddClaim(new Claim(roleType, role));
								}
							}
						}

						return Task.FromResult(0);
					}
				}
			});

			//signin with the app cookie middleware.
			app.SetDefaultSignInAsAuthenticationType(DefaultAuthenticationTypes.ApplicationCookie);
			
			var siteConfig = (TheWebSiteSetup)cont.GetService(typeof(TheWebSiteSetup));

			if (siteConfig.UseLocalAuthServer)
			{
				_log.WriteInformation("Auth setup for local auth server");

				var config = siteConfig.LocalAuthServerOptions;

				app.UseJwtBearerAuthentication(config.BuildAccessTokenReaderOptions());

				config.SetDefaults(app);

				app.UseLocalAuth(config);

				app.UseAutoChallenge(config.BuildAutoChallengeOptions());
			}

			if (!siteConfig.UseLocalAuthServer)
			{
				_log.WriteWarning("Using in-built auto adim login");
				
				app.Use(AssignAdminAccess);
			}
		}

		private static async Task AssignAdminAccess(IOwinContext ctx, Func<Task> next)
		{
			if (ctx?.Authentication?.User?.Identity == null || !ctx.Authentication.User.Identity.IsAuthenticated) {
				
				var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);

				identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, "builtin", null,
					DefaultAuthenticationTypes.ApplicationCookie));

				identity.AddClaim(new Claim(ClaimTypes.Name, "builtin"));

				identity.AddClaim(new Claim(SecurityExtensions.AutoAssignedInbuiltUserClaim.Key, "true"));

				foreach (var item in SecurityExtensions.AllKnownRoles) {
					identity.AddClaim(new Claim(ClaimTypes.Role, item));
				}
				
				ctx.Authentication.SignIn(identity);
				ctx.Authentication.User = new ClaimsPrincipal(identity);
				Thread.CurrentPrincipal = ctx.Authentication.User;
			}

			await next.Invoke();
		}
	}
}