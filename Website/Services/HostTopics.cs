﻿using System.Collections;
using System.Collections.Generic;
using Netteller.Core.Bus;

namespace LaunchSitecore.Services
{
	public class HostTopics : IEnumerable<ITopic>
	{
		public const string DomainTopic = "Netteller.Domain";

		public const string HostTopic = "Netteller.Web";

		public const string AuthServiceTopic = "Netteller.Auth";

		public const string CoreBankingService = "Netteller.Cbs";

		public HostTopics(
			ITopic domainTopic,
			ITopic hostTopic,
			ITopic authTopic,
			ITopic cbTopic)
		{
			Domain = domainTopic;
			Host = hostTopic;
			Auth = authTopic;
			Cbs = cbTopic;
		}

		public ITopic Domain { get; private set; }

		public ITopic Host { get; private set; }

		public ITopic Auth { get; private set; }

		public ITopic Cbs { get; private set; }

		public IEnumerator<ITopic> GetEnumerator()
		{
			yield return Domain;
			yield return Host;
			yield return Auth;
			yield return Cbs;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}