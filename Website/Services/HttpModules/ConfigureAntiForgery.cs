﻿using System.Web;
using System.Web.Helpers;

namespace LaunchSitecore.Services.HttpModules
{
    public class ConfigureAntiForgery : IHttpModule
    {
        private const string _prefix = "Web.";

        public void Init(HttpApplication context)
        {
            SetAntiForgeryCookieNamePrefix();
        }

        private static void SetAntiForgeryCookieNamePrefix()
        {
            if (!AntiForgeryConfig.CookieName.StartsWith(_prefix))
                AntiForgeryConfig.CookieName = _prefix + AntiForgeryConfig.CookieName;
        }

        public void Dispose()
        { }
    }
}