﻿using System;
using System.Net;
using System.Web;
using Netteller.Core.Bus;
using LaunchSitecore.Events;
using NLog;

namespace LaunchSitecore.Services.HttpModules
{
	/// <summary>
	/// Hook into asp.net app lifecycle to publish lifecycle info.
	/// </summary>
	public class HostLifecycleEvents : IHttpModule
	{
		private readonly ILogger _logger;
		private readonly IPublisher _pub;

		public HostLifecycleEvents(ILogger logger, IPublisher pub)
		{
			if (pub == null) {throw new ArgumentNullException("pub");}

			_logger = logger;
			_pub = pub;
		}

		public void Init(HttpApplication context)
		{
			var hostName = Dns.GetHostName();

			_logger.Debug("Web app starting up on {host}", hostName);

			_pub.Send(new HostStartup {
				Name = hostName
			}).Wait();
		}

		public void Dispose()
		{
			var hostName = Dns.GetHostName();

			_logger.Debug("Web app disposing on {host}", hostName);

			_pub.Send(new HostShutdown {
				Name = hostName
			}).Wait();
		}
	}
}