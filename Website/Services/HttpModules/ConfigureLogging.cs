﻿using System;
using System.Web;
using Netteller.Core.NLog;
using LaunchSitecore.Models;

namespace LaunchSitecore.Services.HttpModules
{
	public class ConfigureLogging : IHttpModule
	{
		private readonly LoggingOptions _siteLoggingConfig;

		public ConfigureLogging(TheWebSiteSetup siteConfig)
		{
			if (siteConfig == null) { throw new ArgumentNullException(nameof(siteConfig));}

			if (siteConfig.Logging == null)
			{
				throw new ArgumentNullException(nameof(siteConfig.Logging));
			}

			_siteLoggingConfig = siteConfig.Logging;
		}

		public void Init(HttpApplication context)
		{
			_siteLoggingConfig.Install();	
		}

		public void Dispose()
		{}
	}
}