﻿using Netteller.Core.Bus;
using Netteller.Core.Bus.HandleEvents;
using Netteller.Domain.Events;
using Netteller.Domain.Services;

namespace LaunchSitecore.Services
{
	/// <summary>
	/// defered-forward domain event to bus.
	/// </summary>
	public class DomainPublish : EventForwarder, IDomainPublish
	{
		public DomainPublish(IPublisher innerPublisher)
			:base(innerPublisher)
		{}

		public void Send(IEvent domainEvent)
		{
			base.Send(domainEvent);
		}
	}
}